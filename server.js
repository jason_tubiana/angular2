let express = require('express')
let app = require('express')()
let bodyParser = require('body-parser')
let multer = require('multer')
let session = require('express-session')

// Template
app.set('view engine', 'ejs')

// Middleware
app.use('/assets',express.static('public'))
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(session({
  secret: 'foieafef8efe2ahyjlom3pjtyh42',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}))

// Route
app.get('/', (request, response) => {
    if (request.session.error) {
        response.locals.error = request.session.error
        request.session.error = undefined
    }
    response.render('pages/index', {test: 'Bonjour les gens !'})
})

app.post('/', (request, response) => {
    if (request.body.message === undefined || request.body.message === ''){
        request.session.error = "Il y a une erreur !" 
        response.redirect('/')
    }
})

app.listen(8080)